package BoariuGeorge_Exam;

public class S1 {
}

interface I{

}

class Y{
    B b;
    public void f(){}
}

class B implements I{
    private String param;
    C c;
    B(){
        this.c = new C();
    }
}

class Z{
    public void b(B b){}
    public void g(){}
}

class C{

}

class E{
    B b;
    public void bb(B b){}
}
